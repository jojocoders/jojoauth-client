#Jojonomic Oauth2 Client

This PHP class can be imported to your project to simplify [Jojonomic API call](http://docs.developerjojonomicpro.apiary.io/).

Client ID and Client Secret is needed for using this class.

Include this package in your project by adding this code in your composer.json
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/jojocoders/jojoauth-client.git"
    }
],
```
And also add `"jojocoders/jojoauth-client": "dev-master"` in require section in your composer.json

##Example
```php
<?php
require_once 'vendor\autoload.php';

use Jojonomic\JojonomicClient;

$client = new JojonomicClient('this_is_your_client_id','this_is_your_client_secret');

$response = $client->sendAuthenticatedRequest('POST', '/company/searchReport', [
    'body' => json_encode([
        'company_id' => 1,
        'start_date' => null,
        'end_date' => null,
        'email' => null,
        'listStatus' => null,
        'listGroup' => null,
        'listCategory' => null,
        'isExport' => null,
        'group_by' => null,
        'name' => null,
        'trans_id' => null,
        'trans_desc' => null,
        'pre_desc' => null
    ])
], []);

echo json_encode($response); //$response is always an array
```

##Important Function

###`getTokenString()`
Return `string` token which used to auth the request.

###`sendAuthenticatedRequest($method, $endpoint, $params, $urlParams)`
Return `array` response body from request.

`$method` can be `'GET'` or `'POST'`

`$endpoint` is endpoint without base url, for example `'/company/searchReport'`
 
`$params` is array, contains `'body'`(request body in json), `'headers'`(request header in array key-value)

`$urlParams` is array, contains parameter that will be passed in url

for `POST` request you can see example above, for `GET` request it will be something like this
```php
$response = $client->sendAuthenticatedRequest('GET', '/company/staff', [], [
    'company_id' => 1,
    'group_id' => 1
]);
```