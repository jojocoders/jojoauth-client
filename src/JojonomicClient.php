<?php

namespace Jojonomic;

use Exception;
use League\OAuth2\Client\Provider\AbstractProvider;
use GuzzleHttp\Client as RequestClient;

class JojonomicClient extends AbstractProvider
{
    protected $clientId;
    protected $clientSecret;
    protected $redirectUrl;
    protected $devUrl = 'https://developer-api.jojonomic.com/v0.1';
    protected $accessToken;
    protected $requestClient;

    public function __construct($clientId, $clientSecret, $redirectUrl = '')
    {
        parent::__construct();

        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->redirectUrl = $redirectUrl;
        $this->requestClient = new RequestClient(['base_uri' => $this->devUrl, 'http_errors' => false]);
    }

    public function getBaseAuthorizationUrl()
    {
        return $this->devUrl . '/authorize';
    }

    public function getBaseAccessTokenUrl(array $params)
    {
        return $this->devUrl . '/token';
    }

    public function getResourceOwnerDetailsUrl(\League\OAuth2\Client\Token\AccessToken $token)
    {
        throw new Exception('Not implemented');
    }

    public function getTokenString()
    {
        $this->reactivateToken();
        return $this->accessToken->getToken();
    }

    public function sendAuthenticatedRequest($method, $endpoint, $params = [], $urlParams = [])
    {
        $this->reactivateToken();
        $url = $this->devUrl.$endpoint;
        $url .= '?access_token='.$this->accessToken->getToken();
        if (!empty($urlParams)) {
            foreach ($urlParams as $key => $value) {
                $url .= '&'.urlencode($key).'='.urlencode($value);
            }
        }
        $params['headers'] = isset($params['headers']) ?
            array_merge($params['headers'],['Content-Type' => 'application/json']) : ['Content-Type' => 'application/json'];

        $requestClient = $this->requestClient;
        $response = $requestClient->request($method, $url, $params);
        $statusCode = $response->getStatusCode();

        if ($statusCode >= 200 && $statusCode < 300) {
             return json_decode($response->getBody()->getContents(),true);
        } elseif ($statusCode >= 300 && $statusCode < 400) {
            return [
                'error' => true,
                'message' => 'Redirect happens.'
            ];
        } elseif ($statusCode == 400) {
            return [
                'error' => true,
                'message' => 'Incomplete Params or Bad Request.'
            ];
        } elseif ($statusCode == 401 || $statusCode == 403) {
            return [
                'error' => true,
                'message' => 'Not allowed to access this endpoint.'
            ];
        } elseif ($statusCode == 404) {
            return [
                'error' => true,
                'message' => 'Endpoint not found.'
            ];
        } elseif ($statusCode != 400 && $statusCode != 401 && $statusCode != 403
            && $statusCode != 404 && $statusCode >= 400 && $statusCode < 500) {
            return [
                'error' => true,
                'message' => 'Client error.'
            ];
        } elseif ($statusCode >= 500 && $statusCode < 600) {
            return json_decode($response->getBody()->getContents(),true);
        } else {
            return [
                'error' => true,
                'message' => 'Uncommon error happened.'
            ];
        }
    }

    protected function getDefaultScopes()
    {
        return [];
    }

    protected function checkResponse(\Psr\Http\Message\ResponseInterface $response, $data)
    {
        if ((int) $response->getStatusCode() >= 400) {
            throw new Exception('Error: '.$data['message']);
        }
    }

    protected function createResourceOwner(array $response, \League\OAuth2\Client\Token\AccessToken $token)
    {
        throw new Exception('Not implemented');
    }

    private function reactivateToken()
    {
        if (!isset($this->accessToken) || $this->accessToken->hasExpired()) {
            $this->accessToken = $this->getAccessToken('client_credentials');
        }
    }
}