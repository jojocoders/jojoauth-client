<?php

use PHPUnit\Framework\TestCase;
use Jojonomic\JojonomicClient;

class JojonomicClientTest extends TestCase
{
    public function testGetTokenString()
    {
        $client = new JojonomicClient(
            '9d81eb8f0e9144732d4600a60349bb77',
            'cbe94ff121255827e67c98100f1dee53');

        $token = $client->getTokenString();
        $this->assertNotEquals('', $token);
    }

    public function testSendPostRequest()
    {
        $client = new JojonomicClient(
            '9d81eb8f0e9144732d4600a60349bb77',
            'cbe94ff121255827e67c98100f1dee53');
        $resultBody = $client->sendAuthenticatedRequest('POST', '/company/searchReport', [
            'body' => json_encode([
                'company_id' => 3,
                'start_date' => null,
                'end_date' => null,
                'email' => null,
                'listStatus' => null,
                'listGroup' => null,
                'listCategory' => null,
                'isExport' => null,
                'group_by' => null,
                'name' => null,
                'trans_id' => null,
                'trans_desc' => null,
                'pre_desc' => null
            ])
        ], []);
        $this->assertTrue(is_array($resultBody));
        $this->assertTrue(isset($resultBody['error']));
        $this->assertTrue(isset($resultBody['message']));
    }

    public function testSendGetRequest()
    {
        $client = new JojonomicClient(
            '9d81eb8f0e9144732d4600a60349bb77',
            'cbe94ff121255827e67c98100f1dee53');
        $resultBody = $client->sendAuthenticatedRequest('GET', '/company/staff', [], [
            'company_id' => 3,
            'group_id' => 5
        ]);
        $this->assertTrue(is_array($resultBody));
        $this->assertTrue(isset($resultBody['error']));
        $this->assertTrue(isset($resultBody['message']));
    }
}